## Setup:

  - Ambiente(S.O.): Windows 10  
  - Editor: PyCharm Community Edition 2020.1.1  
  - Editor: IDLE  
  - Interpretador: Python 3.7.1  
  - Framework Appium 1.15.1   
  - Android 8.1.0     
  - Dispositivo físico: Smartphone Samsung   

## Parte 1

### Automação Python Test
  
Foram implementadas duas automações no ambiente Android manipulando os aplicativos: **Play Store** e **Instagram**.

Cenário 1: Download & Install app Android Instagram via Play Store.  
Cenário 2: Login no app Instagram com usuário previamente cadastrado.  
  

## Parte 2

### Algoritmos 

. Primeira solução foi uma implementação de ordenação de string numérica, usando o algoritmo BoobleSort em linguagem Python

. [Refactoring] Segunda solução foi uma implementação, também em linguagem Python, que identifica o **turning number** e retorna o **index** de um **array unimodal** de entrada.
  
