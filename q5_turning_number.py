import sys


def turning_number(arr_uni_modal):
    tn = idx = None # turning number | index
    
    # percorre array ate turning number
    for item in range(1, len(arr_uni_modal) - 1):
        # verifica valor de mudança
        if (arr_uni_modal[item] < arr_uni_modal[item - 1] and arr_uni_modal[item] < arr_uni_modal[item + 1]):
            tn = arr_uni_modal[item] # atribui valor turning number
            break

    if not(tn): return idx # se não encontra turning number

    max_ind = len(arr_uni_modal) # tamanho do array
    idx = arr_uni_modal[0] - arr_uni_modal[max_ind - 1]  # atribui index

    # valida index constante
    for item in range(1, int((max_ind - 1)/2) + 1):
        # index temporario
        idx_tmp = abs(arr_uni_modal[item] - arr_uni_modal[max_ind - item - 1])

        # se index não permanece 
        if idx_tmp != idx:
            return None 
    
    return idx

        
def main():
    arr_entrada = [10, 9, 8, 7, 6, 1, 2, 3, 4, 5]
    print(turning_number(arr_entrada))


if __name__ == "__main__":
    main()
