import sys

# algoritmo de ordenação em bolha
def bubbleSort(lista):
    for passa in range(len(lista)-1,0,-1):
        for i in range(passa):
            if lista[i]>lista[i+1]:
                aux = lista[i]
                lista[i] = lista[i+1]
                lista[i+1] = aux           

# método principal                   
def main():
    # string de numeros de entrada
    str_entrada = "10 8 2 3 5 1"

    # converte string de entrada em lista de numero como string
    lista_str_numeros = str_entrada.split()

    #converte em lista numerica de inteiros
    lista_int_numeros = []
    for item in range(len(lista_str_numeros)):
        lista_int_numeros.append(int(lista_str_numeros[item]))

    #ordena lista numerica
    bubbleSort(lista_int_numeros)

    # torna para string a lista numerica ordenada
    str_saida = ""
    for item in range(len(lista_int_numeros)):
        str_saida += str(lista_int_numeros[item]) + " "
    str_saida = str_saida[:-1]

    # retorna string de saida ordenda em relação a string de entrada
    print(str_saida)

    
if __name__ == "__main__":
    main()
