import time
import unittest
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction


class LoginInstagram(unittest.TestCase):
    desired_capabilities = {}
    driver = None

    def setUp(self):
        # especifica o pacote do app a ser invocado
        self.desired_capabilities['appPackage'] = "com.instagram.android"
        # especifica a Main Activity do app selecionado
        self.desired_capabilities['appActivity'] = "com.instagram.mainactivity.MainActivity"
        #  especifica a platform
        self.desired_capabilities['platformName'] = 'Android'
        # especifica o dispositivo usado (DuT - device under test)
        self.desired_capabilities['deviceName'] = '52038b75fe5ea35f'
        # especifica que o app tem todas as permisssões
        self.desired_capabilities['autoGrantPermissions'] = True
        # especifica o driver passando as 'Desired Capabilities'.
        self.driver = webdriver.Remote("http://localhost:4723/wd/hub", self.desired_capabilities)

    def test_loginInstagram(self):
        # tempo de segurança, pode ser removido/alterado conforme performance do dispositivo
        time.sleep(3)
        # toca no texto/link 'Confirmar' no toast de confirmação dos Termos e Politica de Privacidade
        TouchAction(self.driver).tap(None, 355, 880, 1).perform()

        # abre link tela de Login
        self.driver.find_element_by_id("com.instagram.android:id/log_in_button").click()
        # informa e-mail no campo Login
        self.driver.find_element_by_id("com.instagram.android:id/login_username").send_keys(
            "conta@email.com")
        # informa senha
        self.driver.find_element_by_id("com.instagram.android:id/password").send_keys("senha")
        # clica no botão Entrar
        self.driver.find_element_by_id("com.instagram.android:id/button_text").click()

        # verifica link para Story do usuário
        self.assertEqual(self.driver.find_element_by_id("com.instagram.android:id/username").text, "Seu story")
        # verifica presença avatar
        self.assertEqual(self.driver.find_element_by_id("com.instagram.android:id/avatar_image_view").text,
                         "Story de andrericardo.pro na coluna 0")


if __name__ == '__main__':
    unittest.main()
