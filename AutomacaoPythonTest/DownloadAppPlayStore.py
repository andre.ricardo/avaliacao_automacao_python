import time
import unittest
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class DownloadAppPlayStore(unittest.TestCase):
    desired_capabilities = {}
    driver = None

    def setUp(self):
        # especifica o pacote do app a ser invocado
        self.desired_capabilities['appPackage'] = "com.android.vending"
        # especifica a Main Activity do app selecionado
        self.desired_capabilities['appActivity'] = "com.google.android.finsky.activities.MainActivity"
        #  especifica a platform
        self.desired_capabilities['platformName'] = 'Android'
        # especifica o dispositivo usado (DuT - device under test)
        self.desired_capabilities['deviceName'] = '52038b75fe5ea35f'
        # especifica o driver passando as 'Desired Capabilities'.
        self.driver = webdriver.Remote("http://localhost:4723/wd/hub", self.desired_capabilities)

    # metodo para validar instalação do App Instagram
    def test_installApp(self):
        # aguarda app Play Sore abrir e campo de pesquisa habilitado
        wait = WebDriverWait(self.driver, 30)
        element = wait.until(EC.element_to_be_clickable((By.ID, 'com.android.vending:id/search_bar_hint')))
        element.click()

        # informa nome do app para pesquisa na loja
        search_field = self.driver.find_element_by_id("com.android.vending:id/search_bar_text_input")
        search_field.click()
        search_field.send_keys("instagram")

        # aguarda opções carregarem e pressiona Enter
        time.sleep(2)
        self.driver.press_keycode(66)

        # aguarda botão Instalar estar habilitado e clica nele
        wait = WebDriverWait(self.driver, 30)
        element = wait.until(EC.element_to_be_clickable((By.ID, "com.android.vending:id/right_button")))
        element.click()

        # aguarda botão Cancelar mudar para Abrir
        while True:
            WebDriverWait(self.driver, 30).until(EC.element_to_be_clickable((By.ID, "com.android.vending:id/right_button")))
            btn_open = self.driver.find_element_by_id("com.android.vending:id/right_button")

            # abre app e sai do loop
            if btn_open.text == "Abrir":
                btn_open.click()
                break

        # tempo de segurança, pode ser removido/alterado dependendo da performance do dispositivo
        time.sleep(3)
        # verifica se app instalado
        self.assertEqual(self.driver.is_app_installed('com.instagram.android'), True)

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
